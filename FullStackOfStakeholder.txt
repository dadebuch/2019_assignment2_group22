Prima di poter procede con l'effettiva strategia di elicitazione dei requisiti, abbiamo focalizzato il nostro lavoro, oltre che nel descrivere l'applicazione e le sue funzionalità, nello studio e realizzazione di due attività distinte riguardanti la scoperta del dominio applicativo dell'applicazione considerata.
La prima ha riguardato la scoperta di tutti i possibili stakeholders, cioè tutte quelle persone fisiche o organizzazioni che saranno direttamente o indirettamente interessati all'andamento (positivo o negativo) della nuova applicazione.
Questa attività ha un ruolo fondamentale in quanto le differenti strategie di elicitazione dei requisiti vengono effettuate attraverso l'interazione con i diversi stakeholder, risulta quindi molto importante riuscire ad inviduarli tutti in modo corretto.
La seconda attività è stata invece l'attività di background studies con la quale abbiamo analizzato le informazioni necessarie a capire la fattibilità dell'applicazione ed il suo sviluppo futuro, nell'ottica di intrattenere interazioni proficue con i vari stakelholders nelle fasi successive.

DESCRIZIONE APPLICAZIONE:

L’obiettivo dell’applicazione, commissionata da (?), è quello di aiutare le persone che utilizzano un proprio mezzo di trasporto (macchine e moto) a cercare parcheggio nell’area
di Milano, visto il grande numero di persone che giornalmente affollano la città.

Dopo aver effettuato il login si potrà accedere all’applicazione, si potrà inserire la via o la zona desiderata e verranno mostrati i parcheggi (sia gratuiti sia a pagamento) disponibili nelle vicinanze.
Si può impostare un orario previsto in cui lasciare il parcheggio utilizzato, in modo da attivare automaticamente un timer sul proprio dispositivo; questo al fine di ricordare
fino a quando sarà possibile sostare senza intercorrere in multe (per i parcheggi a pagamento). 
In più, l'applicazione segnerà tramite gps la posizione del veicolo parcheggiato.
Gli utilizzatori potranno, dopo aver usufruito di un parcheggio, lasciare una valutazione e dei commenti riferiti a quel parcheggio così che gli utilizzatori futuri possano avere più informazioni sui diversi parcheggi disponibili.
Sarà presente inoltre una sezione che include notizie relative a chiusure di alcune strade o zone (per esempio a causa di manifestazioni o lavori stradali) al fine di segnalare quali parcheggi 
non saranno disponibili in un determinato lasso di tempo.

Gli utilizzatori saranno di due tipologie:
1.	Standard: che potranno accedere solo alle funzionalità base dell’applicazione tra cui prenotazione e pagamento del parcheggio.
2.	Premium: oltre alle funzionalità standard, questa tipologia di utenti potrà prenotare in anticipo un parcheggio (occupato in quel momento).




LISTA STAKEHOLDERS:

•	AMMINISTRATORE DELEGATO

L'amministratore delegato della compagnia che ha mostrato interesse nel progetto è uno stakeholder attivo; in particolar modo, analizzando e finanziando la realizzazione
del progetto, ha un forte interesse nella realizzazione e nel conseguente successo dello stesso.

Sarà effettuata un'intervista come prima attività (prima di interagire con altri stakeholders) per identificare i principali requisiti generali che dovranno essere garantiti 
all’applicazione. Dopo aver effettuato attività di elicitazione con altri stakeholders, in caso di conflitto con i requisiti emersi dal colloquio con l'amministratore delegato,
verrà svolta una nuova sessione di gruppo per capirne la natura e cercare di risolverli, o trovare un compromesso tra le diverse esigenze dei vari stakeholders.


•	INVESTITORI

Gli investitori sono quegli utenti che hanno deciso di partecipare attivamente alla raccolta fondi per il progetto, avranno accesso alla versione beta dell'applicazione e
successivamente avranno dei vantaggi. 

Queste figure emergono in seguito alla somministrazione del questionario relativo agli utenti, nel caso in cui ci fosse interesse alla partecipazione  attiva alla
realizzazione del progetto. Gli utenti verranno sottoposti ad un nuovo questionario integrativo per analizzare eventuali problematiche e/o vantaggi conseguenti all'investimento.


•	UTENTI

Gli utilizzatori rappresentano la categoria di stakeholder più importanti in quanto sono coloro che utilizzeranno effettivamente l’applicazione.
Inizialmente forniranno un feedback sulle funzionalità iniziali dell'applicazione, ed in un futuro sviluppo contribuiranno con nuovi feedback mirati al cambiamenti e al miglioramenti da apportare all'applicazione.

In questa fase somministreremo un questionario per capire se l’applicazione sia utile e realizzabile, oltre a comprendere se il progetto possa riscontrare interesse.
In questo modo sarà possibile ricavare requisiti mancanti, integrare funzionalità a requisiti esistenti oppure rimuoverne di superflui.
Il questionario ci aiuterà a capire quante persone utilizzerebbero l’applicazione e con quali modalità la utilizzerebbero, oltre che capire le loro preferenze su molti aspetti applicativi 
tra cui ad esempio le modalità di pagamento .


•	TEAM DI SVILUPPO

Il team di sviluppo è composto da programmatori che hanno come scopo la realizzazione e l'integrazione dell’applicazione in tutte le sue componenti. è uno stakeholder interno
fondamentale in quanto, lavorando giornalmente allo sviluppo dell’applicazione, deve tener conto della continua evoluzione dei requisiti emersi in seguito al colloquio con gli altri stakeholder, tra cui l'amministratore delegato. Quest'ultimo avrà il compito di convalidare i vari requisiti emersi dai questionari.

Verrà svolta periodicamente una sessione di gruppo con l'amministratore delegato per evidenziare eventuali incompatibilità tra requisiti e fattibilità degli stessi, consentendo raggiungere un compromesso adeguato.


•	AZIENDE / COMUNE DI MILANO / PROPRIETARI PARCHEGGI PRIVATI

Aziende che hanno sede a Milano, il comune di Milano e proprietari di parcheggi privati sono stakeholders passivi in quanto fornitori di parcheggi.

In particolare:

1.	Le aziende, per esempio, potrebbero avere interesse verso l'applicazione per diminuire il ritardo e lo stress dei dipendenti che cercano parcheggio. Le aziende se lo ritenessero opportuno e vantaggioso potrebbero incentivare l’utilizzo dell’applicazione da parte dei loro dipendenti. 
    
2.	Il comune di Milano potrebbe trarre vantaggio dall'applicazione. Nello specifico diminuzione del traffico dato dalla ricerca di parcheggi ed eliminando parcheggi in posti non autorizzati limitando o rallentando così la circolazione veicolare.
    
3.	I proprietari di parcheggi privati possono mettere a disposizione dell'applicazione i propri parcheggi. 
Potrebbero nascere delle collaborazioni per concedere agli utilizzatori dell’applicazione eventuali sconti/vantaggi.

Verranno sottoposte a queste categorie dei questionari/inteviste per capire il livello di interesse di questi stakeholders e far emergere nuovi ipotetici requisiti.




BACKGROUND STUDY:

Sono state svolte diverse fasi che rientrano nell'attività di background studies:

1) Per prima cosa abbiamo cercato informazioni riguardo la presenza di applicazioni già esistenti nel nostro dominio, in modo da poter capire quali funzionalità e servizi esse offrono agli utenti, per poi poterle sviluppare o addirittura migliorare durante lo sviluppo della nuova applicazione. 
Abbiamo potuto considerare le qualità e i punti di forza di quelle con maggior successo e quelli di debolezza riscontrare dagli utenti in quelle aventi meno successo. 
Sono stati perciò molto importanti anche i commenti e le recensioni (sia positive che negative) lasciate dagli utenti, che hanno permesso di capire se esistono esigenze specifiche o nuove funzionalità che ancora nessuna applicazione esistente abbia implementato.

2) Un ulteriore studio ha comportato l'analisi della piattaforma target (mobile) sulla quale sviluppare l'applicazione. 
Sono stati considerati pro e contro dello sviluppo su tale piattaforma rispetto ad altre (es. desktop) per poter valutare quale sia la scelta migliore e più vantaggiosa.
