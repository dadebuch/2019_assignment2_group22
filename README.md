# Software Development Process - Second Assignment - Group 22
The aim of the project is to define a strategy for acquiring knowledge about a parking application that might be developed in the furure.

#### Contributors:
The strategy has been developed by Bucci Davide (816067), Coelho Vasco (807304), Finati Davide (817508) and Infantino Andrea (816786).

#### Link to the repository:
https://gitlab.com/dadebuch/2019_assignment2_group22/tree/master

#### Strategy:
* **Full set of stakeholders** relevant to the elicitation process
* **Plan** that shows how the different elicitation activities are expected to be performed
* **Workflow** of the elicitation activities to be performed
* **Detailed presentation** of a questionnaire
* **Result** in terms of requirements derived from the questionnaire 

